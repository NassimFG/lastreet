﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Synapse;
using MySql.Data.MySqlClient;

namespace SynapseIHM
{
    public partial class consultationIntervenant : Form
    {

        public consultationIntervenant()
        {

            InitializeComponent();


            dtIntervenant.Rows.Clear();
            List<Intervenant> listeIntervenant = Intervenant.FetchAll();

            foreach (Intervenant unIntervenant in listeIntervenant)
            {
                dtIntervenant.Rows.Add(unIntervenant.Id.ToString(), unIntervenant.Nom.ToString(), unIntervenant.TauxHoraire.ToString());
            }

        }

        private void dtvIntervenant_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dtIntervenant_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            dtIntervenant.Rows.Clear();
            List<Intervenant> listeIntervenant = Intervenant.FetchAll();

            foreach (Intervenant unIntervenant in listeIntervenant)
            {
                dtIntervenant.Rows.Add(unIntervenant.Id.ToString(), unIntervenant.Nom.ToString(), unIntervenant.TauxHoraire.ToString());
            }
            dtIntervenant.Refresh();
            dtIntervenant.Update();
        }
    }
    }
