﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Synapse;

namespace SynapseIHM
{
    public partial class ajoutIntervenant : Form
    {
        public ajoutIntervenant()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void buttonValider_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrWhiteSpace(tbNom.Text) || nudReleveHoraire.Value == 0) 
            {
                string message = "Il faut que les champs soient remplis ou qu'ils aient une valeur.";
                string caption = "Erreur système";
                DialogResult result;

                result = MessageBox.Show(message, caption);
            }

            else
            {
                Intervenant i = new Intervenant();
                i.Nom = tbNom.Text;
                i.TauxHoraire = nudReleveHoraire.Value;
                i.Save();
                MessageBox.Show("L'intervenant a bien été crée !", "Système", MessageBoxButtons.OK, MessageBoxIcon.None);
                this.Close();
            }

            }

        private void fermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ajoutIntervenant_Load(object sender, EventArgs e)
        {

        }
    }
    } 
