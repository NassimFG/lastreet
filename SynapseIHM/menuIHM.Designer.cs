﻿namespace SynapseIHM
{
    partial class menuIHM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(menuIHM));
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuIntervenant = new System.Windows.Forms.Panel();
            this.buttonProjet = new System.Windows.Forms.Button();
            this.blanc = new System.Windows.Forms.Panel();
            this.buttonMission = new System.Windows.Forms.Button();
            this.buttonIntervenant = new System.Windows.Forms.Button();
            this.panelIntervenant = new System.Windows.Forms.Panel();
            this.buttonConsultationIntervenant = new System.Windows.Forms.Button();
            this.buttonAjoutIntervenant = new System.Windows.Forms.Button();
            this.buttonAjoutMission = new System.Windows.Forms.Button();
            this.blanc2 = new System.Windows.Forms.Panel();
            this.buttonAjoutProjet = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panelIntervenant.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.menuIntervenant);
            this.panel1.Controls.Add(this.buttonProjet);
            this.panel1.Controls.Add(this.blanc);
            this.panel1.Controls.Add(this.buttonMission);
            this.panel1.Controls.Add(this.buttonIntervenant);
            this.panel1.Location = new System.Drawing.Point(-12, -29);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(876, 113);
            this.panel1.TabIndex = 0;
            // 
            // menuIntervenant
            // 
            this.menuIntervenant.ForeColor = System.Drawing.Color.Black;
            this.menuIntervenant.Location = new System.Drawing.Point(158, 114);
            this.menuIntervenant.Name = "menuIntervenant";
            this.menuIntervenant.Size = new System.Drawing.Size(200, 100);
            this.menuIntervenant.TabIndex = 1;
            // 
            // buttonProjet
            // 
            this.buttonProjet.FlatAppearance.BorderSize = 0;
            this.buttonProjet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonProjet.Font = new System.Drawing.Font("Agency FB", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonProjet.ForeColor = System.Drawing.Color.Black;
            this.buttonProjet.Location = new System.Drawing.Point(523, 24);
            this.buttonProjet.Name = "buttonProjet";
            this.buttonProjet.Size = new System.Drawing.Size(177, 89);
            this.buttonProjet.TabIndex = 3;
            this.buttonProjet.Text = "PROJET";
            this.buttonProjet.UseVisualStyleBackColor = true;
            this.buttonProjet.Click += new System.EventHandler(this.button2_Click_1);
            this.buttonProjet.MouseEnter += new System.EventHandler(this.buttonProjet_Enter);
            // 
            // blanc
            // 
            this.blanc.Location = new System.Drawing.Point(13, 113);
            this.blanc.Name = "blanc";
            this.blanc.Size = new System.Drawing.Size(851, 436);
            this.blanc.TabIndex = 2;
            // 
            // buttonMission
            // 
            this.buttonMission.FlatAppearance.BorderSize = 0;
            this.buttonMission.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMission.Font = new System.Drawing.Font("Agency FB", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMission.ForeColor = System.Drawing.Color.Black;
            this.buttonMission.Location = new System.Drawing.Point(346, 24);
            this.buttonMission.Name = "buttonMission";
            this.buttonMission.Size = new System.Drawing.Size(177, 89);
            this.buttonMission.TabIndex = 2;
            this.buttonMission.Text = "MISSION";
            this.buttonMission.UseVisualStyleBackColor = true;
            this.buttonMission.Click += new System.EventHandler(this.buttonMission_Click);
            this.buttonMission.MouseEnter += new System.EventHandler(this.buttonMission_Enter);
            // 
            // buttonIntervenant
            // 
            this.buttonIntervenant.FlatAppearance.BorderSize = 0;
            this.buttonIntervenant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonIntervenant.Font = new System.Drawing.Font("Agency FB", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonIntervenant.ForeColor = System.Drawing.Color.Black;
            this.buttonIntervenant.Location = new System.Drawing.Point(169, 24);
            this.buttonIntervenant.Name = "buttonIntervenant";
            this.buttonIntervenant.Size = new System.Drawing.Size(177, 89);
            this.buttonIntervenant.TabIndex = 1;
            this.buttonIntervenant.Text = "INTERVENANT";
            this.buttonIntervenant.UseVisualStyleBackColor = true;
            this.buttonIntervenant.Click += new System.EventHandler(this.buttonIntervenant_Click);
            this.buttonIntervenant.MouseEnter += new System.EventHandler(this.buttonIntervenant_Enter);
            // 
            // panelIntervenant
            // 
            this.panelIntervenant.BackColor = System.Drawing.Color.White;
            this.panelIntervenant.Controls.Add(this.buttonConsultationIntervenant);
            this.panelIntervenant.Controls.Add(this.buttonAjoutIntervenant);
            this.panelIntervenant.Location = new System.Drawing.Point(157, 84);
            this.panelIntervenant.Name = "panelIntervenant";
            this.panelIntervenant.Size = new System.Drawing.Size(177, 101);
            this.panelIntervenant.TabIndex = 1;
            // 
            // buttonConsultationIntervenant
            // 
            this.buttonConsultationIntervenant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonConsultationIntervenant.Font = new System.Drawing.Font("Agency FB", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonConsultationIntervenant.Location = new System.Drawing.Point(0, 50);
            this.buttonConsultationIntervenant.Name = "buttonConsultationIntervenant";
            this.buttonConsultationIntervenant.Size = new System.Drawing.Size(177, 51);
            this.buttonConsultationIntervenant.TabIndex = 1;
            this.buttonConsultationIntervenant.Text = "CONSULTATION";
            this.buttonConsultationIntervenant.UseVisualStyleBackColor = true;
            this.buttonConsultationIntervenant.Click += new System.EventHandler(this.buttonConsultationIntervenant_Click);
            // 
            // buttonAjoutIntervenant
            // 
            this.buttonAjoutIntervenant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAjoutIntervenant.Font = new System.Drawing.Font("Agency FB", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAjoutIntervenant.Location = new System.Drawing.Point(0, -1);
            this.buttonAjoutIntervenant.Name = "buttonAjoutIntervenant";
            this.buttonAjoutIntervenant.Size = new System.Drawing.Size(177, 51);
            this.buttonAjoutIntervenant.TabIndex = 0;
            this.buttonAjoutIntervenant.Text = "AJOUT";
            this.buttonAjoutIntervenant.UseVisualStyleBackColor = true;
            this.buttonAjoutIntervenant.Click += new System.EventHandler(this.buttonAjoutIntervenant_Click_1);
            // 
            // buttonAjoutMission
            // 
            this.buttonAjoutMission.BackColor = System.Drawing.Color.White;
            this.buttonAjoutMission.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAjoutMission.Font = new System.Drawing.Font("Agency FB", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAjoutMission.Location = new System.Drawing.Point(334, 83);
            this.buttonAjoutMission.Name = "buttonAjoutMission";
            this.buttonAjoutMission.Size = new System.Drawing.Size(177, 51);
            this.buttonAjoutMission.TabIndex = 2;
            this.buttonAjoutMission.Text = "AJOUT";
            this.buttonAjoutMission.UseVisualStyleBackColor = false;
            this.buttonAjoutMission.Click += new System.EventHandler(this.buttonAjoutMission_Click);
            // 
            // blanc2
            // 
            this.blanc2.BackColor = System.Drawing.Color.LightGray;
            this.blanc2.Location = new System.Drawing.Point(-19, 84);
            this.blanc2.Name = "blanc2";
            this.blanc2.Size = new System.Drawing.Size(883, 448);
            this.blanc2.TabIndex = 4;
            this.blanc2.MouseEnter += new System.EventHandler(this.blanc2_Enter);
            // 
            // buttonAjoutProjet
            // 
            this.buttonAjoutProjet.BackColor = System.Drawing.Color.White;
            this.buttonAjoutProjet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAjoutProjet.Font = new System.Drawing.Font("Agency FB", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAjoutProjet.Location = new System.Drawing.Point(511, 83);
            this.buttonAjoutProjet.Name = "buttonAjoutProjet";
            this.buttonAjoutProjet.Size = new System.Drawing.Size(177, 51);
            this.buttonAjoutProjet.TabIndex = 3;
            this.buttonAjoutProjet.Text = "AJOUT";
            this.buttonAjoutProjet.UseVisualStyleBackColor = false;
            this.buttonAjoutProjet.Click += new System.EventHandler(this.buttonAjoutProjet_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(834, 30);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(25, 25);
            this.button1.TabIndex = 5;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // menuIHM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 516);
            this.Controls.Add(this.blanc2);
            this.Controls.Add(this.buttonAjoutMission);
            this.Controls.Add(this.buttonAjoutProjet);
            this.Controls.Add(this.panelIntervenant);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "menuIHM";
            this.Text = "menuIHM";
            this.Load += new System.EventHandler(this.menuIHM_Load);
            this.panel1.ResumeLayout(false);
            this.panelIntervenant.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonIntervenant;
        private System.Windows.Forms.Button buttonProjet;
        private System.Windows.Forms.Button buttonMission;
        private System.Windows.Forms.Panel menuIntervenant;
        private System.Windows.Forms.Panel panelIntervenant;
        private System.Windows.Forms.Panel blanc;
        private System.Windows.Forms.Panel blanc2;
        private System.Windows.Forms.Button buttonAjoutIntervenant;
        private System.Windows.Forms.Button buttonConsultationIntervenant;
        private System.Windows.Forms.Button buttonAjoutMission;
        private System.Windows.Forms.Button buttonAjoutProjet;
        private System.Windows.Forms.Button button1;
    }
}