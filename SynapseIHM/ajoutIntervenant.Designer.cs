﻿namespace SynapseIHM
{
    partial class ajoutIntervenant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ajoutIntervenant));
            this.labelAjoutIntervenant = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbNom = new System.Windows.Forms.TextBox();
            this.buttonValider = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.nudReleveHoraire = new System.Windows.Forms.NumericUpDown();
            this.fermer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudReleveHoraire)).BeginInit();
            this.SuspendLayout();
            // 
            // labelAjoutIntervenant
            // 
            this.labelAjoutIntervenant.AutoSize = true;
            this.labelAjoutIntervenant.BackColor = System.Drawing.Color.Transparent;
            this.labelAjoutIntervenant.Font = new System.Drawing.Font("Agency FB", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAjoutIntervenant.ForeColor = System.Drawing.Color.Black;
            this.labelAjoutIntervenant.Location = new System.Drawing.Point(62, 26);
            this.labelAjoutIntervenant.Name = "labelAjoutIntervenant";
            this.labelAjoutIntervenant.Size = new System.Drawing.Size(298, 39);
            this.labelAjoutIntervenant.TabIndex = 0;
            this.labelAjoutIntervenant.Text = "AJOUTER UN INTERVENANT";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Agency FB", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(42, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "NOM";
            // 
            // tbNom
            // 
            this.tbNom.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbNom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbNom.Location = new System.Drawing.Point(84, 112);
            this.tbNom.Name = "tbNom";
            this.tbNom.Size = new System.Drawing.Size(123, 20);
            this.tbNom.TabIndex = 2;
            // 
            // buttonValider
            // 
            this.buttonValider.BackColor = System.Drawing.Color.ForestGreen;
            this.buttonValider.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonValider.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.buttonValider.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleGreen;
            this.buttonValider.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonValider.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonValider.Location = new System.Drawing.Point(299, 170);
            this.buttonValider.Name = "buttonValider";
            this.buttonValider.Size = new System.Drawing.Size(100, 31);
            this.buttonValider.TabIndex = 3;
            this.buttonValider.Text = "VALIDER";
            this.buttonValider.UseVisualStyleBackColor = false;
            this.buttonValider.Click += new System.EventHandler(this.buttonValider_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Agency FB", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(224, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 24);
            this.label2.TabIndex = 4;
            this.label2.Text = "TAUX HORAIRE";
            // 
            // nudReleveHoraire
            // 
            this.nudReleveHoraire.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.nudReleveHoraire.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudReleveHoraire.Location = new System.Drawing.Point(320, 112);
            this.nudReleveHoraire.Name = "nudReleveHoraire";
            this.nudReleveHoraire.Size = new System.Drawing.Size(40, 20);
            this.nudReleveHoraire.TabIndex = 6;
            // 
            // fermer
            // 
            this.fermer.BackColor = System.Drawing.Color.Transparent;
            this.fermer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("fermer.BackgroundImage")));
            this.fermer.FlatAppearance.BorderSize = 0;
            this.fermer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fermer.Location = new System.Drawing.Point(377, 0);
            this.fermer.Name = "fermer";
            this.fermer.Size = new System.Drawing.Size(25, 25);
            this.fermer.TabIndex = 7;
            this.fermer.UseVisualStyleBackColor = false;
            this.fermer.Click += new System.EventHandler(this.fermer_Click);
            // 
            // ajoutIntervenant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(402, 204);
            this.Controls.Add(this.fermer);
            this.Controls.Add(this.nudReleveHoraire);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonValider);
            this.Controls.Add(this.tbNom);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelAjoutIntervenant);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ajoutIntervenant";
            this.Text = "ajoutIntervenant";
            this.Load += new System.EventHandler(this.ajoutIntervenant_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudReleveHoraire)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelAjoutIntervenant;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbNom;
        private System.Windows.Forms.Button buttonValider;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudReleveHoraire;
        private System.Windows.Forms.Button fermer;
    }
}