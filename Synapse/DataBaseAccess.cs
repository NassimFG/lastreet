﻿
using MySql.Data.MySqlClient;

namespace Synapse
{
    class DataBaseAccess
    {
        public static MySqlConnection getOpenMySqlConnection()
        {
            MySqlConnection msc = new MySqlConnection("Database=synapse;Data Source=172.16.50.52;User Id=grp3;Password=grp3;Ssl Mode=None;charset=utf8");
            msc.Open();
            return msc;
        }
    }
}

//Avec Abstraction -> Diminue le COUPLAGE 

//public static IDbConnection Connexion { get; set; }

//public static IDbDataParameter CodeParam(string paramName, object value)
//{
//    IDbCommand commandSql = Connexion.CreateCommand();
//    IDbDataParameter parametre = commandSql.Crea teParameter();
//    parametre.ParameterName = paramName;
//    parametre.Value = value;
//    return parametre;
//}