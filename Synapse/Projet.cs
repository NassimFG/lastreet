﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace Synapse
{
    /// <summary>
    /// Représente un projet clients de l'entreprise
    /// </summary>
    public class Projet
    {
        /// <summary>
        /// Identifiant du projet
        /// </summary>
        public short Id { get; private set; } // Primary Key (Bd)
        /// <summary>
        /// Nom du projet
        /// </summary>
        public string NomProjet { get; set; }
        /// <summary>
        /// Date de début du projet
        /// </summary>
        public DateTime DateDebutProjet { get; set; } 
        /// <summary>
        /// Date de fin du projet
        /// </summary>
        public DateTime DateFinProjet { get; set; }
        /// <summary>
        /// Prix facturé
        /// </summary>
        public decimal PrixFactureMO { get; set; }
        /// <summary>
        /// Missions réalisées dans le cadre du projet
        /// </summary>
        public List<Mission> Missions { get; set; }

        /// <summary>
        /// Ajoute une mission au projet
        /// </summary>
        /// <param name="uneMission"></param>
        public void AjouterMission(Mission uneMission)
        {
            if(!Missions.Contains(uneMission))
            {
                Missions.Add(uneMission);
                uneMission.Projet = this;
            }
        }

        /// <summary>
        /// Supprime une mission du projet
        /// </summary>
        /// <param name="uneMission"></param>
        public void SupprimerMission(Mission uneMission)
        {
            Missions.Remove(uneMission);
            uneMission.Projet = null;
        }

        /// <summary>
        /// Initialise un nouveau projet
        /// </summary>
        public Projet()
        {
            Id = -1;
            NomProjet = "non défini";
            DateDebutProjet = DateTime.Now;
            DateFinProjet = DateTime.Now.AddMonths(3);
            PrixFactureMO = 0;
            Missions = new List<Mission>();
        }

        /// <summary>
        /// Retourne un projet
        /// </summary>
        /// <param name="idProjet">L'identifiant d'un projet</param>
        /// <returns>Une référence de projet ou null si l'identifiant ne correspond à aucun projet</returns>                  
        public static Projet Fetch(int idProjet)
        {
            Projet unProjet = null;
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _selectByIdSql;
            commandSql.Parameters.Add(new MySqlParameter("?id", idProjet));
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                unProjet = new Projet();
                unProjet.Id = Convert.ToInt16(jeuEnregistrements["id"].ToString());
                unProjet.NomProjet = jeuEnregistrements["nomProjet"].ToString();
                long dateDebutProjet = Convert.ToInt64(jeuEnregistrements["dateDebutProjet"].ToString());
                unProjet.DateDebutProjet = new DateTime(dateDebutProjet);
                long dateFinProjet = Convert.ToInt64(jeuEnregistrements["dateFinProjet"].ToString());
                unProjet.DateFinProjet = new DateTime(dateFinProjet);
                string prixFactureMO = jeuEnregistrements["prixFacture"].ToString();
                unProjet.PrixFactureMO = Convert.ToDecimal(prixFactureMO);
                unProjet.Missions = Mission.FetchAllByProjet(unProjet);
            }
            openConnection.Close();
            return unProjet;
        }
        
        /// <summary>
        /// Retourne l'ensemble des projets de l'entreprise 
        /// </summary>
        /// <returns>Une collection de projets</returns>
        public static List<Projet> FetchAll()
        {
            List<Projet> projets = null;
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _selectSql;           
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Projet unProjet = new Projet();
                unProjet.Id = Convert.ToInt16(jeuEnregistrements["id"].ToString());
                unProjet.NomProjet = jeuEnregistrements["nomProjet"].ToString();
                long dateDebutProjet = Convert.ToInt64(jeuEnregistrements["dateDebutProjet"].ToString());
                unProjet.DateDebutProjet = new DateTime(dateDebutProjet);
                long dateFinProjet = Convert.ToInt64(jeuEnregistrements["dateFinProjet"].ToString());
                unProjet.DateFinProjet = new DateTime(dateFinProjet);
                string prixFactureMO = jeuEnregistrements["prixFacture"].ToString();
                unProjet.PrixFactureMO = Convert.ToDecimal(prixFactureMO);
                unProjet.Missions = Mission.FetchAllByProjet(unProjet);
                projets.Add(unProjet);
            }
            openConnection.Close();
            return projets;
        }

        /// <summary>
        /// Sauvegarde le projet courant
        /// </summary>
        public void Save()
        {
            if (Id == -1)
            {
                Insert();
            }
            else
            {
                Update();

            }
            SaveMission();
        }

        private void SaveMission()
        {
            foreach (Mission uneMission in Missions)
            {
                uneMission.Save();
            }
        }

        private void Update()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(new MySqlParameter("?id", Id));
            commandSql.Parameters.Add(new MySqlParameter("?dateDebutProjet", DateDebutProjet.Ticks));
            commandSql.Parameters.Add(new MySqlParameter("?nomProjet", NomProjet));
            commandSql.Parameters.Add(new MySqlParameter("?dateFinProjet", DateFinProjet.Ticks));
            commandSql.Parameters.Add(new MySqlParameter("?prixFacture", PrixFactureMO));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            openConnection.Close();

        }

        private void Insert()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _insertSql;
            commandSql.Parameters.Add(new MySqlParameter("?dateDebutProjet", DateDebutProjet.Ticks));
            commandSql.Parameters.Add(new MySqlParameter("?nomProjet", NomProjet));
            commandSql.Parameters.Add(new MySqlParameter("?dateFinProjet", DateFinProjet.Ticks));
            commandSql.Parameters.Add(new MySqlParameter("?prixFacture", PrixFactureMO));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            Id = Convert.ToInt16(commandSql.LastInsertedId);
            openConnection.Close();
        }

       

        private static string _selectSql =
          "SELECT id ,  nomProjet ,  dateDebutProjet, dateFinProjet, prixFacture FROM projet";

        private static string _selectByIdSql =
            "SELECT id ,  nomProjet ,  dateDebutProjet, dateFinProjet, prixFacture FROM projet WHERE id = ?id ";

        private static string _updateSql =
            "UPDATE projet set nomProjet=?nomProjet , dateDebutProjet=?dateDebutProjet, dateFinProjet=?dateFinProjet, prixFacture=?prixFacture  WHERE id=?id ";

        private static string _insertSql =
            "INSERT INTO projet (nomProjet,dateDebutProjet,dateFinProjet,prixFacture) VALUES (?nomProjet,?dateDebutProjet,?dateFinProjet,?prixFacture)";

        private static string _deleteByIdSql =
            "DELETE FROM projet WHERE id = ?id";

        private static string _getMissions =
            "SELECT id from mission WHERE idProjet=?idProjet";
    }
}
