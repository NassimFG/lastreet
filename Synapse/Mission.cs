﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace Synapse
{
    /// <summary>
    /// Représente une mission réalisée dans le cadre d'un projet
    /// <!-- public permet un accès entre des classes de différents espace de nom -->
    /// </summary>
    public class Mission 
    { 
        #region Modele Objet
        /// <summary>
        /// L'identifiant de la mission
        /// </summary>
        public short Id { get; private set; } // Primary Key (Bd)
        /// <summary>
        /// Le nom de la mission
        /// </summary>
        public string Nom { get; set; }
        /// <summary>
        /// La description de la mission
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Le nombres d'heures prévues pour la réalisation de la mission
        /// </summary>
        public decimal NbHeuresPrevues { get; set; }
        /// <summary>
        /// représente l'ensemble des relevés d'heures travaillés. Un relévé associe un nombre d'heures travaillés pour une date donnée
        /// </summary>
        public Dictionary<TimeSpan, int> ReleveHoraire { get; set; } //table Releve {association entre Date<non implémentée> et Mission}
        /// <summary>
        /// L'intervenant en charge de la mission
        /// </summary>
        public Intervenant Intervenant { get; set; }//Foreign Key
        /// <summary>
        /// Le projet auquel se ratache la mission
        /// </summary>
        public Projet Projet { get; set; } //Foreign Key

        /// <summary>
        /// Compare un objet transmis en paramètre avec l'objet courant. La comparaison est effectuée sur l'identifiant de l'objet.
        /// </summary>
        /// <param name="obj">L'objet à comparer (seul un objet de type Mission sera comparé avec la mission courante</param>
        /// <returns>Vrai si l'identifiant correspond, faux dans le cas contraire</returns>
        public override bool Equals(object obj)
        {
            bool equals = false;
            Mission m = obj as Mission;
            if (m != null)
            {
                if (m.Id == Id)
                {
                    equals = true;
                }
            }
            return equals;
        }

        #endregion 

        #region Constructeur(s).
        /// <summary>
        /// Initialise une mission
        /// </summary>
        public Mission()
        {
            Id = -1;//Identifie un contact non référencé dans la base de données
            Description = "";
            Nom = "";
            ReleveHoraire = new Dictionary<TimeSpan, int>();
        }
        #endregion

        #region Méthodes d'accès aux données

        /// <summary>
        /// retourne une mission de l'entreprise
        /// </summary>
        /// <param name="idContact">La valeur de la clé primaire</param>
        public static Mission Fetch(int idMission)
        {
            Mission uneMission = null;
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _selectByIdSql;
            commandSql.Parameters.Add(new MySqlParameter("?id", idMission));
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                uneMission = new Mission();
                uneMission.Id = Convert.ToInt16(jeuEnregistrements["id"].ToString());//Lecture d'un champ de l'enregistrement
                uneMission.Nom = jeuEnregistrements["nom"].ToString();
                uneMission.Description = jeuEnregistrements["description"].ToString();
                string nbHeuresPrevues = jeuEnregistrements["nbHeuresPrevues"].ToString();
                uneMission.NbHeuresPrevues = Convert.ToDecimal(nbHeuresPrevues);
                short idIntervenant = Convert.ToInt16(jeuEnregistrements["idIntervenant"].ToString());
                Intervenant unIntervenant = Intervenant.Fetch(idIntervenant);
                short idProjet = Convert.ToInt16(jeuEnregistrements["idProjet"]);
                Projet unProjet = Projet.Fetch(idProjet);
                uneMission.Projet = unProjet;
                uneMission.ReleveHoraire = Mission.GetReleveByIdMission(uneMission.Id);
                uneMission.Intervenant = unIntervenant;

            }
            openConnection.Close();
            return uneMission;
        }

        /// <summary>
        /// Sauvegarde ou met à jour une Mission
        /// </summary>
        public void Save()
        {
            if (Projet == null)
            {
                throw new Exception("Une mission doit être liée à un projet pour être sauvegarder");
            }
            else if (Id == -1)
            {
                Insert();
            }
            else
            {
                Update();

            }
            SaveReleveByIdMission(Id, ReleveHoraire);
        }

        /// <summary>
        /// Supprime la mission 
        /// </summary>
        public void Delete()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _deleteByIdSql;
            commandSql.Parameters.Add(new MySqlParameter("?id", Id));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            Id = -1;
            openConnection.Close();
        }



        /// <summary>
        /// Retourne l'ensemble des missions de l'entreprise
        /// </summary>
        /// <returns>Une collection de missions</returns>
        public static List<Mission> FetchAll()
        {
            List<Mission> resultat = new List<Mission>();
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _selectSql;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Mission uneMission = new Mission();
                string idMission = jeuEnregistrements["id"].ToString();
                uneMission.Id = Convert.ToInt16(idMission);
                uneMission.Nom = jeuEnregistrements["nom"].ToString();
                string nbHeuresPrevues = jeuEnregistrements["nbHeuresPrevues"].ToString();
                uneMission.NbHeuresPrevues = Convert.ToDecimal(nbHeuresPrevues);
                Int16 idIntervenant = Convert.ToInt16(jeuEnregistrements["idIntervenant"].ToString());
                Intervenant unIntervenant = Intervenant.Fetch(idIntervenant);
                Int16 idProjet = Convert.ToInt16(jeuEnregistrements["idProjet"]);
                Projet unProjet = Projet.Fetch(idProjet);
                uneMission.ReleveHoraire = Mission.GetReleveByIdMission(uneMission.Id);
                uneMission.Intervenant = unIntervenant;
                uneMission.Projet = unProjet;
                resultat.Add(uneMission);
            }
            openConnection.Close();
            return resultat;
        }

        public static List<Mission> FetchAllByProjet(Projet unProjet)
        {
            List<Mission> collectionMission = new List<Mission>();
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _selectByIdProjet;
            commandSql.Parameters.Add(new MySqlParameter("?idProjet", unProjet.Id));
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Mission uneMission = new Mission();
                string idMission = jeuEnregistrements["id"].ToString();
                uneMission.Id = Convert.ToInt16(idMission);
                uneMission.Nom = jeuEnregistrements["nom"].ToString();
                string nbHeuresPrevues = jeuEnregistrements["nbHeuresPrevues"].ToString();
                uneMission.NbHeuresPrevues = Convert.ToDecimal(nbHeuresPrevues);
                Int16 idIntervenant = Convert.ToInt16(jeuEnregistrements["idIntervenant"].ToString());
                Intervenant unIntervenant = Intervenant.Fetch(idIntervenant);
                uneMission.ReleveHoraire = Mission.GetReleveByIdMission(uneMission.Id);
                uneMission.Intervenant = unIntervenant;
                uneMission.Projet = unProjet;
                collectionMission.Add(uneMission);
            }
            openConnection.Close();
            return collectionMission;
        }

        private void Update()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(new MySqlParameter("?id", Id));
            commandSql.Parameters.Add(new MySqlParameter("?nbHeuresPrevues", NbHeuresPrevues));
            commandSql.Parameters.Add(new MySqlParameter("?nom", Nom));
            commandSql.Parameters.Add(new MySqlParameter("?description", Description));
            commandSql.Parameters.Add(new MySqlParameter("?idIntervenant", Intervenant.Id));
            commandSql.Parameters.Add(new MySqlParameter("?idProjet", Projet.Id));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            openConnection.Close();

        }

        private void Insert()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _insertSql;

            commandSql.Parameters.Add(new MySqlParameter("?nbHeuresPrevues", NbHeuresPrevues));
            commandSql.Parameters.Add(new MySqlParameter("?nom", Nom));
            commandSql.Parameters.Add(new MySqlParameter("?description", Description));
            commandSql.Parameters.Add(new MySqlParameter("?idIntervenant", Intervenant.Id));
            commandSql.Parameters.Add(new MySqlParameter("?idProjet", Projet.Id));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();

            Id = Convert.ToInt16(commandSql.LastInsertedId);
            openConnection.Close();
        }

        private static Dictionary<TimeSpan, int> GetReleveByIdMission(int idMission)
        {
            Dictionary<TimeSpan, int> releve = new Dictionary<TimeSpan, int>();
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _getReleveByIdMission;
            commandSql.Parameters.Add(new MySqlParameter("?idMission", idMission));
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                string date = jeuEnregistrements["date"].ToString();
                Int64 dateReleve = Convert.ToInt64(date);
                string nbHeures = jeuEnregistrements["nbHeures"].ToString();
                int nbHeureReleve = Convert.ToInt32(nbHeures);
                releve.Add(new TimeSpan(dateReleve), nbHeureReleve);
            }
            openConnection.Close();

            return releve;
        }

        private static void SaveReleveByIdMission(int idMission, Dictionary<TimeSpan, int> releve)
        {
            DeleteReleveByIdMission(idMission);
            foreach (KeyValuePair<TimeSpan, int> kvp in releve)
            {
                InsertReleveByIdMission(idMission, kvp);
            }
        }

        private static void DeleteReleveByIdMission(int idMission)
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _deleteReleveByIdMission;
            commandSql.Parameters.Add(new MySqlParameter("?idMission", idMission));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            openConnection.Close();
        }

        private static void InsertReleveByIdMission(int idMission, KeyValuePair<TimeSpan, int> unReleve)
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _insertUnReleveByIdMission;
            commandSql.Parameters.Add(new MySqlParameter("?date", unReleve.Key.Ticks));
            commandSql.Parameters.Add(new MySqlParameter("?nbHeures", unReleve.Value));
            commandSql.Parameters.Add(new MySqlParameter("?idMission", idMission));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            openConnection.Close();
        }



        #endregion

        #region Champs à portée classe contenant l'ensemble des requêtes d'accès aux données
        private static string _selectSql =
            "SELECT id ,  nom ,  description, nbHeuresPrevues, idIntervenant, idProjet FROM mission";

        private static string _selectByIdSql =
            "SELECT id ,  nom ,  description, nbHeuresPrevues, idIntervenant, idProjet FROM mission WHERE id = ?id ";

        private static string _updateSql =
            "UPDATE mission SET nom = ?nom, description=?description , nbHeuresPrevues=?nbHeuresPrevues, idIntervenant=?idIntervenant, idProjet=?idProjet  WHERE id=?id ";

        private static string _insertSql =
            "INSERT INTO mission (nom,description,nbHeuresPrevues,idIntervenant,idProjet) VALUES (?nom,?description,?nbHeuresPrevues,?idIntervenant,?idProjet)";

        private static string _deleteByIdSql =
            "DELETE FROM mission WHERE id = ?id";

        private static string _getReleveByIdMission =
        "SELECT date, nbHeures FROM releve where idMission=?idMission ";

        private static string _deleteReleveByIdMission =
            "DELETE FROM releve WHERE idMission = ?idMission";

        private static string _insertUnReleveByIdMission =
            "INSERT INTO releve (idMission,nbHeures,date) VALUES (?idMission,?nbHeures,?date)";

        private static string _selectByIdProjet =
            "SELECT id ,  nom ,  description, nbHeuresPrevues, idIntervenant, idProjet FROM mission where idProjet = ?idProjet";

        #endregion
    }
} 
